# Embedded Checkout

This project demonstrates an example integration of an embedded checkout system using Flask and Peach Payments.

## Prerequisites

- Python 3.13.0
- Flask
- Requests
- python-dotenv

## Setup

1. Clone the repository:

    ```sh
    git clone https://gitlab.com/p2886/checkout-samples
    cd python/Embedded%20Checkout
    ```

2. Create a virtual environment using pyenv:

   2.1 Create a virtual environment named `venv` (install pyenv if you don't have it)
    ```sh
    pyenv virtualenv 3.13.0 venv
    ```
   2.2 Activate the virtual environment
   ```sh
    pyenv activate venv
    ```
   2.3 Use pip to install poetry
    ```sh
    pip install poetry
    ```

3. Install the required dependencies:

    ```sh
    poetry install --no-root
    ```

4. Create a `.env` file in the `Embedded Checkout` directory and populate it with your credentials. You can use the `.env.example` file as a template:

    ```sh
    cp .env.example .env
    ```

    Update the `.env` file with your actual credentials.

## Running the application

1. Start the Flask application:

    ```sh
    python app.py
    ```

2. Open your web browser and navigate to `http://127.0.0.1:5000` to see the embedded checkout page.

## Project structure

- `app.py`: Main application file that contains the Flask routes and logic for obtaining access tokens and checkout IDs.
- `.env.example`: Example environment variables file.
- `templates/index.html`: HTML template for the checkout page.

## Endpoints

- `/`: Renders the checkout page.
- `/checkout`: Handles the checkout process by obtaining an access token and checkout ID, then returns a JSON response.

## Logic
- The fetch("/checkout", { method: "POST" }) function call in the index.html file sends a POST request to the /checkout endpoint of the Flask application, to generate a new checkoutId and get an access token.
- The get_checkout_id() function retrieves a checkout ID from the Peach Payment's Checkout endpoint.
```python
def get_checkout_id(access_token: str):
```
- The `Checkout.initiate()` function is part of the Peach Payments Embedded Checkout SDK. This SDK is included in your `index.html` file via the script tag:

```html
<script src="{{ checkoutJs }}"></script>
```
The `checkoutJs` variable is set to the URL of the Peach Payments Embedded Checkout JavaScript file. This script provides the `Checkout` object and its methods, including `initiate()`.