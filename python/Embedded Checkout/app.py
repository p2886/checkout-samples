import os
import random
import string
from http import HTTPStatus

import requests
from dotenv import load_dotenv
from flask import Flask, jsonify, render_template, request

# Load environment variables from .env file
load_dotenv()

# Assign environment variables to Python variables
ENTITY_ID = str(os.getenv("ENTITY_ID"))
CLIENT_ID = str(os.getenv("CLIENT_ID"))
CLIENT_SECRET = str(os.getenv("CLIENT_SECRET"))
MERCHANT_ID = str(os.getenv("MERCHANT_ID"))

DOMAIN = str(os.getenv("DOMAIN"))
AUTHENTICATION_ENDPOINT = str(os.getenv("AUTHENTICATION_ENDPOINT"))
CHECKOUT_ENDPOINT = str(os.getenv("CHECKOUT_ENDPOINT"))
CHECKOUT_JS = str(os.getenv("CHECKOUT_JS"))

# Checkout uses a POST request to redirect the customer to this URL after the customer completes checkout.
# Must be a valid URL that can be accessed through a browser.
REDIRECT_URL = str(os.getenv("REDIRECT_URL"))

app = Flask(__name__)


def get_access_token() -> str:
    """
    Retrieves an access token from the authentication endpoint.

    This function sends a POST request to the authentication endpoint with the
    client ID, client secret, and merchant ID to obtain an access token. The
    access token is required for subsequent API calls to the Checkout endpoint.

    Returns:
        str: The access token if the request is successful, otherwise an empty string.
    """
    response = requests.post(
        AUTHENTICATION_ENDPOINT,
        headers={"Content-Type": "application/json"},
        json={"clientId": CLIENT_ID, "clientSecret": CLIENT_SECRET, "merchantId": MERCHANT_ID},
    )

    # Check if the request was successful
    if response.status_code == HTTPStatus.OK:
        # Extract the access token from the response
        access_token = response.json().get("access_token")
    else:
        # Handle the error
        access_token = ""

    return access_token


def get_checkout_id(access_token: str) -> str:
    """
    Retrieves a checkout ID from the Checkout endpoint.

    This function sends a POST request to the Checkout endpoint with the necessary
    headers and body to obtain a checkout ID. The checkout ID is used for processing
    payments on the frontend.

    Args:
        access_token (str): The access token required for authentication.

    Returns:
        str: The checkout ID if the request is successful, otherwise an empty string.
    """

    # Call the Checkout endpoint to get a checkout ID for use on the frontend.
    headers = {
        "Content-Type": "application/json",
        "Origin": DOMAIN,
        "Referer": DOMAIN,
        "Authorization": f"Bearer {access_token}",
        "accessToken": access_token,
    }
    body = {
        "authentication": {"entityId": ENTITY_ID},
        "merchantTransactionId": "INV-0000001",
        "amount": 100,
        "currency": "ZAR",
        "paymentType": "DB",
        "nonce": "".join(random.choices(string.ascii_uppercase + string.digits, k=16)),
        "shopperResultUrl": REDIRECT_URL,
    }

    response = requests.post(
        f"{CHECKOUT_ENDPOINT}/v2/checkout",
        headers=headers,
        json=body,
    )
    # Check if the request was successful
    if response.status_code == HTTPStatus.OK:
        # Extract the access token from the response
        checkout_id = response.json().get("checkoutId")
    else:
        # Handle the error
        checkout_id = ""
    return checkout_id


@app.route("/")
def home():
    """
    Renders the home page of the Flask application.

    This function handles the root URL ("/") and renders the "index.html" template.
    It also passes the `CHECKOUT_JS` variable to the template for use in the frontend.

    Returns:
        Response: The rendered HTML template for the home page.
    """
    return render_template("index.html", checkoutJs=CHECKOUT_JS)


@app.route("/checkout", methods=["POST"])
def checkout():
    """
    Handles the checkout process by obtaining an access token and a checkout ID.

    This function is triggered by a POST request to the "/checkout" endpoint. It first
    retrieves an access token by calling the `get_access_token` function. Then, it uses
    this access token to obtain a checkout ID by calling the `get_checkout_id` function.
    The function returns a JSON response containing the checkout ID and entity ID.

    Returns:
        Response: A JSON response with the checkout ID and entity ID.
    """
    access_token = get_access_token()
    checkout_id = get_checkout_id(access_token)
    # make sure the keys are checkoutId and entityId
    response = {"message": "OK", "checkoutId": checkout_id, "entityId": ENTITY_ID}
    return jsonify(response)


@app.route("/result", methods=["POST"])
def result():
    return render_template("index.html")


if __name__ == "__main__":
    app.run(debug=True)
