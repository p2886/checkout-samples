"""
    Read more about Checkout parameters at `https://developer.peachpayments.com/reference/post_checkout-initiate`
"""
import hashlib
import hmac
import os
import random
import string
from http import HTTPStatus

import requests
from dotenv import load_dotenv
from flask import Flask, jsonify, redirect, render_template, request

# Load environment variables from .env file
load_dotenv()

# Assign environment variables to Python variables
ENTITY_ID = str(os.getenv("ENTITY_ID"))
CHECKOUT_ENDPOINT = str(os.getenv("CHECKOUT_ENDPOINT"))
SECRET_KEY = str(os.getenv("SECRET_KEY"))
DOMAIN = str(os.getenv("DOMAIN"))
REDIRECT_URL = str(os.getenv("REDIRECT_URL"))
WEBHOOK_URL = str(os.getenv("WEBHOOK_URL"))

app = Flask(__name__)


def __to_str(value) -> str:
    if isinstance(value, float):
        return f"{value:.2f}"
    return str(value)

def _create_signature(options: dict, secret_key: str) -> str:
    """
    To generate the signature, all payment parameters must be in alphabetical order,
    concatenated, without any spaces or special characters, and signed with the secret token as the key.
    https://developer.peachpayments.com/docs/checkout-authentication#how-to-generate-a-signature-hmac-sha256
    """
    data = {k: v for (k, v) in options.items() if v}  # map to ensure the dict is not empty
    data_list = [str(key) + __to_str(data[key]) for key in sorted(data)]  # sort alphabetically and join key-value pairs
    data_str = "".join(data_list)  # join all the elements of the list

    # encode key and msg to bytes
    key = secret_key.encode("utf-8")
    msg = data_str.encode("utf-8")

    # create a new HMAC object
    return hmac.new(key=key, msg=msg, digestmod=hashlib.sha256).hexdigest()


def get_hosted_checkout_redirection_url(
    entity_id: str,
    secret_key: str,
    domain: str,
    redirect_url: str,
    webhook_url: str,
):
    """
        Creates a redirection link for the hosted checkout.

        This function generates a redirection link for the hosted checkout by sending a POST request
        to the checkout endpoint with the necessary headers and body. The body includes the authentication
        entity ID, amount, currency, merchant transaction ID, nonce, payment type, shopper result URL,
        and notification URL. The body is signed using the secret key to generate a signature.

        Args:
            entity_id (str): The entity ID for authentication.
            secret_key (str): The secret key used to sign the request.
            domain (str): The domain from which the request originates.
            redirect_url (str): The URL to which the customer is redirected after checkout.
            webhook_url (str): The URL to which webhook notifications are sent.

        Returns:
            dict: The response from the checkout endpoint, containing the redirection link.
        """
    headers = {
        "Content-Type": "application/json",
        "Origin": domain,
        "Referer": domain,
    }

    body = {
        "authentication.entityId": entity_id,
        "amount": "100",
        "currency": "ZAR",
        "merchantTransactionId": "INV-0000001",
        "nonce": "".join(random.choices(string.ascii_uppercase + string.digits, k=16)),
        "paymentType": "DB",
        "shopperResultUrl": redirect_url,
        "notificationUrl": webhook_url,
    }

    # create signature by hashing the body with the secret key
    body["signature"] = _create_signature(body, secret_key)
    return requests.post(f"{CHECKOUT_ENDPOINT}/checkout/initiate", headers=headers, json=body)



@app.route("/")
def home():
    return render_template("index.html")


@app.route("/checkout", methods=["POST"])
def checkout():
    response = get_hosted_checkout_redirection_url(
        entity_id=ENTITY_ID,
        secret_key=SECRET_KEY,
        domain=DOMAIN,
        redirect_url=REDIRECT_URL,
        webhook_url=WEBHOOK_URL,
    )

    if response.status_code == HTTPStatus.CREATED:
        return redirect(response.json().get("redirectUrl"))

    return {"status": response.status_code, "message": response.json().get("message")}



@app.route("/result", methods=["POST"])
def result():
    """
    The checkout API redirects the user to this API endpoint after processing.
    For now the user is redirected to index.html
    """
    return render_template("index.html")



@app.route("/webhook", methods=["POST"])
def webhook():
    """
    Webhooks are HTTP callbacks that deliver notification messages for events ( refund, debit, reversal etc. ).
    Checkout sends a webhook to this API endpoint.
    """
    print("Webhook body", request.json)
    return "", HTTPStatus.OK


if __name__ == "__main__":
    app.run(debug=True)
