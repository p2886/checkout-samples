# Hosted Checkout

This project demonstrates an example integration of a hosted checkout system using Flask and Peach Payments.

## Prerequisites

- Python 3.13.0
- Flask
- Requests
- python-dotenv

## Setup

1. Clone the repository:

    ```sh
    git clone https://gitlab.com/p2886/checkout-samples
    cd python/Hosted%20Checkout%20-%20Redirect
    ```

2. Create a virtual environment using pyenv:

   2.1 Create a virtual environment named `venv` (install pyenv if you don't have it)
    ```sh
    pyenv virtualenv 3.13.0 venv
    ```
   2.2 Activate the virtual environment
   ```sh
    pyenv activate venv
    ```
   2.3 Use pip to install poetry
    ```sh
    pip install poetry
    ```

3. Install the required dependencies:

    ```sh
    poetry install
    ```

4. Create a `.env` file in the `Hosted Checkout` directory and populate it with your credentials. You can use the `.env.example` file as a template:

    ```sh
    cp .env.example .env
    ```

    Update the `.env` file with your actual credentials.

## Running the application

1. Start the Flask application:

    ```sh
    python app.py
    ```

2. Open your web browser and navigate to `http://127.0.0.1:5000` to see the hosted checkout page.

## Project structure

- `app.py`: Main application file that contains the Flask routes and logic for obtaining access tokens and checkout IDs.
- `.env.example`: Example environment variables file.
- `templates/index.html`: HTML template for the checkout page.

## Endpoints

- `/`: Renders the checkout page.
- `/checkout`: Handles the checkout process by obtaining an access token and checkout ID, then returns a JSON response.
- `/result`: Handles the result of the checkout process.
- `/webhook`: Handles webhook notifications.

## Logic
- The "checkout", "POST" form method in the index.html file sends a POST request to the /checkout endpoint of the Flask application. This endpoint is defined in the app.py file as follows:
```python
@app.route("/checkout", methods=["POST"])
def checkout():
    response = get_hosted_checkout_redirection_url(
        entity_id=ENTITY_ID,
        secret_key=SECRET_KEY,
        domain=DOMAIN,
        redirect_url=REDIRECT_URL,
        webhook_url=WEBHOOK_URL,
    )

    if response.status_code == HTTPStatus.CREATED:
        return redirect(response.json().get("redirectUrl"))

    return {"status": response.status_code, "message": response.json().get("message")}
```
- The get_hosted_checkout_redirection_url function in the app.py file sends a POST request to the Peach Payments API to obtain a hosted checkout redirection URL. The response contains the URL to which the user should be redirected to complete the payment process.