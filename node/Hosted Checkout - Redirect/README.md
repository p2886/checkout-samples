# Hosted Checkout - Redirect sample integration

This sample [Redirect Checkout](https://developer.peachpayments.com/docs/checkout-hosted) integration has a **Pay now** button that generates a checkout instance and redirects to it.

## Requirements

- NodeJS v18+
- ngrok (ngrok.com) account (for redirect and webhooks while developing locally)
- Peach Payments account with the following Checkout credentials:
  - `entityId`
  - `secret`

## Setup

- Run `npm install` to install all the required dependencies.
- Create a copy of `.env.example` and rename it to `.env`.
  - Replace the ENTITY_ID, SECRET_KEY, and DOMAIN values in the `.env` file with your Checkout credentials.
  - Replace the REDIRECT and WEBHOOK URLs in the `.env` file with the ngrok URL from the first step of the Run section.

## Run

- `ngrok http 3000` - Set up a tunnel for ngrok, this is required for webhooks and redirect from Checkout.
- `npm start` - This starts an ExpressJs service.
