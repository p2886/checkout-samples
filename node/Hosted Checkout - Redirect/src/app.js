require("dotenv").config();

const express = require("express");
const app = express();
const ejs = require("ejs");
const { createCheckout } = require("./createCheckout");
const path = require("path");

/**
 * The below values can be retrieved from the Checkout screen on Dashboard.
 */
const entityId = process.env.ENTITY_ID;
const secretKey = process.env.SECRET_KEY;
const domain = process.env.DOMAIN;
const redirectUrl = process.env.REDIRECT_URL;
const webhookUrl = process.env.WEBHOOK_URL;

app.engine("html", ejs.renderFile);
app.set("view engine", "html");
app.set("views", path.join(__dirname, "/views"));

app.use(express.urlencoded({ extended: true }));

app.get("/", function (req, res) {
  return res.render("index.ejs");
});

app.post("/checkout", async function (req, res) {
  const link = await createCheckout(
    entityId,
    secretKey,
    domain,
    redirectUrl,
    webhookUrl
  );

  if (link && link.redirectUrl) {
    return res.redirect(link.redirectUrl);
  }

  return res.status(500).json(link);
});

app.post("/result", async function (req, res) {
  console.log("Result body", req.body);

  return res.render("index.ejs");
});

app.post("/webhook", async function (req, res) {
  console.log("Webhook", req.body);

  return res.status(200).send();
});

app.listen(3000, function () {
  console.log("Server is running on localhost:3000");
});
