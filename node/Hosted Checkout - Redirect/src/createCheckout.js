const CryptoJS = require("crypto-js");

const checkoutEndpoint = process.env.CHECKOUT_ENDPOINT;

const signOptions = (options, secretKey) => {
  const joinedOptions = Object.keys(options)
    .sort()
    .map((key) => `${key}${options[key] ?? ""}`)
    .join("");

  options["signature"] = CryptoJS.HmacSHA256(joinedOptions, secretKey).toString(
    CryptoJS.enc.Hex
  );
};

const createCheckout = async (
  entityId,
  secretKey,
  domain,
  redirectUrl,
  webhookUrl
) => {
  const myHeaders = new Headers();
  myHeaders.append("Origin", domain);
  myHeaders.append("Referer", domain);
  myHeaders.append("Content-Type", "application/json");

  /**
   * Read more about Checkout parameters at `https://developer.peachpayments.com/reference/post_checkout-initiate`
   */
  const options = {
    "authentication.entityId": entityId,
    amount: (10.0).toFixed(2),
    currency: "ZAR",
    merchantTransactionId: "INV-0000001",
    nonce: (Math.random() * 100000).toString(),
    paymentType: "DB",
    shopperResultUrl: redirectUrl,

    notificationUrl: webhookUrl,
  };

  signOptions(options, secretKey);

  const response = await fetch(`${checkoutEndpoint}/checkout/initiate`, {
    method: "POST",
    headers: myHeaders,
    body: JSON.stringify(options),
  })
    .then((response) => response.json())
    .catch((error) => console.log("error", error));

  console.log(response);

  return response;
};

module.exports = { createCheckout };
