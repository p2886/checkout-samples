# Embedded Checkout sample integration

This sample Embedded Checkout integration has a **Pay now** button that generates a checkout ID and initiates checkout.

You can [theme the checkout](https://developer.peachpayments.com/docs/checkout-embedded-sdk-reference#theming) and [customise the order of payment methods](https://developer.peachpayments.com/docs/checkout-embedded-sdk-reference#customise-payment-method-order). Embedded Checkout exposes [events](https://developer.peachpayments.com/docs/checkout-embedded-sdk-reference#events) for the custom handling of `Completed`, `Cancelled`, and `Expired` scenarios.

## Requirements

- NodeJS V18+
- Peach Payments account with the following Checkout credentials:
  - `entityId`
  - `clientId`
  - `clientSecret`
  - `merchantId`
- Domain that Peach Payments has allowlisted

## Setup

- Run `npm install` to install all the required dependencies.
- Create a copy of `.env.example` and rename it to `.env`, then replace the values in the `.env` file with your Checkout credentials and allowlisted domain.

## Run

- Run `npm start` to start the ExpressJs service.
- Open `http://localhost:3000` in a browser.

## Authentication

- Access tokens should be cached and reused until they expire. Refer to `https://developer.peachpayments.com/docs/checkout-embedded-authentication` for more information.

## Options

Embedded Checkout supports various optional settings.

### Theme

Refer to `https://developer.peachpayments.com/docs/checkout-embedded-sdk-reference#theming` for more information about theming.

- `brand` - Styles user interface items such as loaders, currency, and hover effects.
- `cards` - Styles payment method card backgrounds and hover backgrounds.

### Payment method order

Refer to `https://developer.peachpayments.com/docs/checkout-embedded-sdk-reference#customise-payment-method-order` for more information about payment method ordering.

- Ordering is done by assigning a rank to a payment method name.
- Consult our developer documentation for more information about payment method names `https://developer.peachpayments.com/docs/pp-payment-methods#south-africa`.

### Customise Checkout user interface

Refer to `https://developer.peachpayments.com/docs/checkout-embedded-sdk-reference#customise-checkout-user-interface` for more information on customising the Checkout user interface:

- For the payment method selection page and the landing page for each payment method (when you click into a payment method), you can remove the cancel button and amount.
- In addition to the above, you can remove the card brand field from the card payment form.
